//
//  MCManager.m
//  MCDemo
//
//  Created by Gabriel Theodoropoulos on 1/7/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "MCManager.h"


@implementation MCManager

-(id)init{
    self = [super init];
    
    if (self) {
        _peerID = nil;
        _session = nil;
        _browser = nil;
        _advertiser = nil;
    }
    
    return self;
}


#pragma mark - Public method implementation

/**
 *  The setupPeerAndSessionWithDisplayName method goes and sets the name of the session and the peer
 *  from the displayName String pointer. First we set the Peer ID variable which is the MCPeerID, 
 *  representing a peer in the Multipeer session. Then it sets the session variable which is the MCSession,
 *  enabling and managing communication among peers in the Multipeer Connectivity session. It does this by 
 *  using the peerID to connect to the desired display name. We then set up the delegation of the session
 *  by setting it to this class.
 */
-(void)setupPeerAndSessionWithDisplayName:(NSString *)displayName{
    _peerID = [[MCPeerID alloc] initWithDisplayName:displayName];
    
    _session = [[MCSession alloc] initWithPeer:_peerID];
    _session.delegate = self;
}

/**
 *  The setupMCBrowser method goes and sets up the MCBrowserViewController for the MCManager class we
 *  initialize it with the service type of basic text communication and file transfer, we use the MCSession
 *  as well to set it up.
 */
-(void)setupMCBrowser{
    _browser = [[MCBrowserViewController alloc] initWithServiceType:@"chat-files" session:_session];
}

/**
 *  The advertiseSelf method receives a boolean called shouldAdvertise method, this is from the user coming
 *  from the ConnectionsView. If the user Says yes then the MCManager sets the MCAdvertiserAssistant 
 *  initializing it with the service type of chat-files which is just text communication and file transfer,
 *  the discoveryInfo to nil and the session to the session that was created by the user using the displayName.
 *  We then set the advertiser that we just created to start. If the user says no to advertising then the advertiser
 *  is told to stop and is set to nil.
 */
-(void)advertiseSelf:(BOOL)shouldAdvertise{
    if (shouldAdvertise) {
        _advertiser = [[MCAdvertiserAssistant alloc] initWithServiceType:@"chat-files"
                                                           discoveryInfo:nil
                                                                 session:_session];
        [_advertiser start];
    }
    else{
        [_advertiser stop];
        _advertiser = nil;
    }
}


#pragma mark - MCSession Delegate method implementation

/**
 *  The session method is there to create a post to the notification center, this allows for 
 *  information in regards to if the user change his session state. It receives the peer ID and
 *  the MCSessionState.
 */
-(void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state{
    NSDictionary *dict = @{@"peerID": peerID,
                           @"state" : [NSNumber numberWithInt:state]
                           };
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MCDidChangeStateNotification"
                                                        object:nil
                                                      userInfo:dict];
}

/**
 *  The session method is there to create a post to the notification center stating that the user
 *  received data from the user. It receives the data and the peerID from which the data was sent.
 */
-(void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID{
    NSDictionary *dict = @{@"data": data,
                           @"peerID": peerID
                           };
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MCDidReceiveDataNotification"
                                                        object:nil
                                                      userInfo:dict];
}

/**
 *  The session method is there to create a post to the notification center stating that the peer 
 *  did receive the resource notification and then update the user on the progress on how that 
 *  resource is currently being downloaded.
 */
-(void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress{
    
    NSDictionary *dict = @{@"resourceName"  :   resourceName,
                           @"peerID"        :   peerID,
                           @"progress"      :   progress
                           };
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MCDidStartReceivingResourceNotification"
                                                        object:nil
                                                      userInfo:dict];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [progress addObserver:self
                   forKeyPath:@"fractionCompleted"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    });
}

/**
 *  The session method is there to create a Notification center to state the the object was fully 
 *  received. It receives the session the name of the resource, the peer thats sending it the local
 *  url and if there is an error.
 */
-(void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error{
    
    NSDictionary *dict = @{@"resourceName"  :   resourceName,
                           @"peerID"        :   peerID,
                           @"localURL"      :   localURL
                           };
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"didFinishReceivingResourceNotification"
                                                        object:nil
                                                      userInfo:dict];
    
}

/**
 *  The session method goes and does nothing .... nothing at all
 */
-(void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID{
    
}

/**
 *  The observeValueForKeyPath method goes and creates a notification center stating that a peice of 
 *  information is still being downloaded.
 */
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MCReceivingProgressNotification"
                                                        object:nil
                                                      userInfo:@{@"progress": (NSProgress *)object}];
}

@end
